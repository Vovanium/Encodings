with AUnit.Simple_Test_Cases;
with AUnit.Assertions;
use  AUnit.Assertions;

with Encodings.Converters;
with Encodings.Generic_Converters;
with Encodings.Unicode.UTF_8.From_UTF_32;
with Encodings.Unicode.UTF_16.From_UTF_32;
with Encodings.Unicode.UTF_32.From_UTF_8;

with Interfaces;
use  Interfaces;

package body Encodings.Unicode.Converter_Test is

	type Test is new AUnit.Simple_Test_Cases.Test_Case with record
		Chunk_Size : Positive;
	end record;

	function Name (T : Test) return AUnit.Message_String;

	procedure Run_Test (T : in out Test);

	function Name (T : Test) return AUnit.Message_String is
	begin
		return AUnit.Format ("Encodings.Unicode.Converter_Test with Chunk_Size = " & Positive'Image (T.Chunk_Size));
	end Name;

	generic
		type Character_Type is (<>);
		type String_Type is array (Positive range <>) of Character_Type'Base;
	procedure Gen_Assert_Eq (
		A, B : String_Type;
		Message : String := "");

	procedure Gen_Assert_Eq (
		A, B : String_Type;
		Message : String := "")
	is
	begin
		for I in 0 .. Natural'Min (A'Length, B'Length) - 1 loop
			Assert (A (A'First + I) = B (B'First + I), "Not equal characters: "
			& Integer'Image (Character_Type'Pos (A (A'First + I)))
			& " and "
			& Integer'Image (Character_Type'Pos (B (B'First + I)))
			& " at offset " & Natural'Image (I)
			& " " & Message);
		end loop;
		Assert (A'Length = B'Length, "Not equal lengths "
		& Integer'Image (A'Length)
		& " and "
		& Integer'Image (A'Length)
		& " " & Message);
	end Gen_Assert_Eq;

	procedure Assert_Eq is new Gen_Assert_Eq (UTF_8.UTF_8_Character, UTF_8.UTF_8_String);
	procedure Assert_Eq is new Gen_Assert_Eq (UTF_16.UTF_16_Character, UTF_16.UTF_16_String);
	procedure Assert_Eq is new Gen_Assert_Eq (UTF_32.UTF_32_Character, UTF_32.UTF_32_String);

	generic
		with package Base is new Encodings.Generic_Converters (<>);
		with procedure Assert_Eq (A, B : Base.Target_String; Message : String) is <>;
	procedure Gen_Test (
		T      : Test;
		Conv   : in out Base.Converter'Class;
		Source : Base.Source_String;
		Target : Base.Target_String);

	procedure Gen_Test (
		T      : Test;
		Conv   : in out Base.Converter'Class;
		Source : Base.Source_String;
		Target : Base.Target_String)
	is
		B : Base.Target_String (Target'Range);
		SI, TI : Natural;
	begin
		Conv.Process (Source, SI, B, TI);
		Assert_Eq (B, Target, "Strings must match");
		Assert (SI = Source'Last, "Whole buffer must be read");
		Assert (TI = B'Last, "Whole buffer must be written");
	end Gen_Test;

	procedure Test_8_32  is new Gen_Test (Base => Converters.Narrow_to_Wide_Wide);
	procedure Test_32_8  is new Gen_Test (Base => Converters.Wide_Wide_to_Narrow);
	procedure Test_32_16 is new Gen_Test (Base => Converters.Wide_Wide_to_Wide);

	procedure Test_One (
		T : in out Test;
		NS : UTF_8.UTF_8_String;
		WS : UTF_16.UTF_16_String;
		ZS : UTF_32.UTF_32_String)
	is
		Conv_8_32  : UTF_32.From_UTF_8.Converter;
		Conv_32_8  : UTF_8.From_UTF_32.Converter;
		Conv_32_16 : UTF_16.From_UTF_32.Converter;
	begin
		Test_8_32  (T, Conv_8_32,  NS, ZS);
		Test_32_8  (T, Conv_32_8,  ZS, NS);
		Test_32_16 (T, Conv_32_16, ZS, WS);
	end Test_One;

	type Hex is ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F');

	type Hex_String is array (Positive range <>) of Hex;

	function UC (H : Hex_String) return Code_Point;

	function UC (H : Hex_String) return Code_Point
	is (if H'Length = 0 then
		0
	else
		Hex'Pos (H (H'Last)) + 16 * UC (H (H'First .. H'Last - 1)));

	function U (H : Hex_String) return UTF_8.UTF_8_Character is (UTF_8.UTF_8_Character'Val (UC (H)));
	function U (H : Hex_String) return UTF_16.UTF_16_Character is (UTF_16.UTF_16_Character'Val (UC (H)));
	function U (H : Hex_String) return UTF_32.UTF_32_Character is (UTF_32.UTF_32_Character'Val (UC (H)));

	procedure Run_Test (T : in out Test) is
	begin
		Test_One (T, "", "", "");
		Test_One (T, "abc", "abc", "abc");
		Test_One (T, U ("D0") & U ("A9"), "Щ", "Щ");
		Test_One (T, U ("E2") & U ("89") & U ("A0"), "≠", "≠");
		Test_One (T, U ("F0") & U ("9F") & U ("90") & U ("B1"), U ("D83D") & U ("DC31"), "" & U ("01F431")); -- cat face
	end Run_Test;

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (new Test'(AUnit.Simple_Test_Cases.Test_Case with Chunk_Size => 1_000));
		return R;
	end Suite;

end Encodings.Unicode.Converter_Test;
