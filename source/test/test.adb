with AUnit.Reporter.Text;
with AUnit.Run;

with Encodings.Test;

procedure Test is
	procedure Runner is new AUnit.Run.Test_Runner (Encodings.Test.Suite);
	Reporter : AUnit.Reporter.Text.Text_Reporter;
begin
	Runner (Reporter);
end Test;
