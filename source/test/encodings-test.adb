with Encodings.Unicode.Test;

package body Encodings.Test is

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (Encodings.Unicode.Test.Suite);
		return R;
	end Suite;

end Encodings.Test;
