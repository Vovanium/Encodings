with Encodings.Unicode.Converter_Test;

package body Encodings.Unicode.Test is

	function Suite return Access_Test_Suite is
		R : Access_Test_Suite := new Test_Suite;
	begin
		R.Add_Test (Encodings.Unicode.Converter_Test.Suite);
		return R;
	end Suite;

end Encodings.Unicode.Test;
