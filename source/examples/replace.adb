--
-- Program reads standard input, replaces string in first parameter
-- with one in second, then writes to standard output
--

with Ada.Streams, Ada.Text_IO, Ada.Text_IO.Text_Streams;
use  Ada.Streams, Ada.Text_IO, Ada.Text_IO.Text_Streams;
with Ada.Command_Line;
use  Ada.Command_Line;
with Encodings.Generic_Single_Replace;
with Encodings.Utility, Encodings.Converters;
use  Encodings.Utility, Encodings.Converters;

procedure Replace is

	Input_Stream    : Stream_Access := Stream (Standard_Input);
	Output_Stream   : Stream_Access := Stream (Standard_Output);
	Input_Buffer    : String (1 .. 100);
	Output_Buffer   : String (1 .. 100);
	Input_Last,
	Input_Read_Last : Natural;
	Output_Last     : Natural;
begin
	if Argument_Count < 2 then
		return;
	end if;
	declare
		package Coders is new Encodings.Generic_Single_Replace (
			Character_Type => Character,
			String_Type => String,
			Original => Argument (1),
			Replacement => Argument (2),
			Converter_Base  => Encodings.Converters.Narrow_To_Narrow);
		Coder : Coders.Converter;
	begin
		while not End_Of_File (Standard_Input) loop
			Read_String (Input_Stream.all, Input_Buffer, Input_Read_Last);
			Input_Last := Input_Buffer'First - 1;
			while Input_Last < Input_Read_Last loop
				Coder.Process (Input_Buffer (Input_Last + 1 .. Input_Read_Last),
					Input_Last, Output_Buffer, Output_Last);
				String'Write (Output_Stream, Output_Buffer (Output_Buffer'First .. Output_Last));
			end loop;
		end loop;
	end;
end Replace;
