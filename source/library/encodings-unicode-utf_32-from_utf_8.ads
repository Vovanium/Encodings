with Encodings.Converters;
with Encodings.Unicode.UTF_8;
use  Encodings.Unicode.UTF_8;

package Encodings.Unicode.UTF_32.From_UTF_8 is

	type Converter is new Converters.Narrow_to_Wide_Wide.Converter with private;

	overriding function In_Initial_State (State : Converter) return Boolean;

	overriding procedure Reset (State : in out Converter);

	overriding procedure Process (
		State       : in out Converter;
		Source      : in     UTF_8_String;
		Source_Last :    out Natural;
		Target      :    out UTF_32_String;
		Target_Last :    out Natural);

private
	subtype Sequence_Count is Integer range 0 .. 4;

	type Converter is new Converters.Narrow_to_Wide_Wide.Converter
	with record
		N    : Sequence_Count  := 0; -- Sequence length (only for legality checking)
		L    : Sequence_Count  := 0; -- Remaining
		Code : Integer         := 0;
	end record;

end Encodings.Unicode.UTF_32.From_UTF_8;
