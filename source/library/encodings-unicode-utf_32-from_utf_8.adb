package body Encodings.Unicode.UTF_32.From_UTF_8 is

	function In_Initial_State (State : Converter) return Boolean
	is (State.L = 0);

	procedure Reset (State : in out Converter) is
	begin
		State.L := 0;
		State.Code := 0;
	end Reset;

	procedure Process (
		State       : in out Converter;
		Source      : in     UTF_8_String;
		Source_Last :    out Natural;
		Target      :    out Wide_Wide_String;
		Target_Last :    out Natural)
	is
		C : Character;
	begin
		Source_Last := 0;
		Target_Last := 0;
		while Source_Last < Source'Last and Target_Last < Target'Last loop
			Source_Last := Source_Last + 1;
			C := Source (Source_Last);
			if State.L = 0 then
				case C is
				when Single_Byte_Character =>
					State.N := 1;
					Target_Last := Target_Last + 1;
					Target (Target_Last) := Wide_Wide_Character'Val (
						Character'Pos (C));
				when Initial_2_Byte =>
					State.N := 2;
					State.L := 1;
					State.Code := Character'Pos (C) - 16#C0#;
				when Initial_3_Byte =>
					State.N := 3;
					State.L := 2;
					State.Code := Character'Pos (C) - 16#E0#;
				when Initial_4_Byte =>
					State.N := 4;
					State.L := 3;
					State.Code := Character'Pos (C) - 16#F0#;
				when others =>
					raise Encoding_Error with "Not an initial byte";
				end case;
			else
				if C not in Extension_Byte then
					raise Encoding_Error with "Not an extension byte";
				end if;
				State.L := State.L - 1;
				State.Code := State.Code * 64 + (Character'Pos (C) - 16#80#);
				if State.L = 0 then
					case State.N is
					when 2 =>
						null; -- No checks needed
					when 3 =>
						if State.Code not in 16#0800# .. 16#FFFF# then
							raise Encoding_Error with "Not the shortest encoding";
						end if;
					when 4 =>
						if State.Code not in 16#1_0000# .. 16#10_FFFF# then
							raise Encoding_Error with "Not the shortest encoding"
							& "or out of unicode range";
						end if;
					when others =>
						raise Program_Error; -- This shouldn't happen
					end case;
					Target_Last := Target_Last + 1;
					Target (Target_Last) := Wide_Wide_Character'Val (State.Code);
				end if;
			end if;
		end loop;
	end Process;

end Encodings.Unicode.UTF_32.From_UTF_8;
