with Interfaces;
with Ada.Strings.UTF_Encoding;

package Encodings.Unicode with Pure is

	Code_Point_Last : constant := 16#10FFFF#;

	Byte_Order_Mark_Code : constant := 16#FEFF#;

private
	subtype Code_Point is Interfaces.Unsigned_32 range 0 .. Code_Point_Last;

	subtype Basic_Multilingual_Plane is Code_Point
		range 0 .. 16#FFFF#;

	subtype Sequence_Count is Integer range 0 .. 4;

end Encodings.Unicode;
