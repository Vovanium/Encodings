package body Encodings.Line_Endings.Generic_Add_CR is

	function In_Initial_State (State : Converter) return Boolean
	is (State.State = Initial);

	procedure Reset (
		This : in out Converter)
	is
	begin
		This.State := Initial;
	end;

	procedure Process (
		This          : in out Converter;
		Source        : in     String_Type;
		Source_Cursor :    out Natural;
		Target        :    out String_Type;
		Target_Cursor :    out Natural
	) is
		C : Character_Type;
	begin
		Source_Cursor := Source'First - 1;
		Target_Cursor := Target'First - 1;
		if This.State = Need_LF then
			if Target_Cursor >= Target'Last then -- Wonder of anyone would supply zero-size string but precaution is must
				return;
			end if;
			Target_Cursor := Target_Cursor + 1;
			Target (Target_Cursor) := Line_Feed;
			This.State := Initial;
		end if;
		while Source_Cursor < Source'Last and Target_Cursor < Target'Last loop
			Source_Cursor := Source_Cursor + 1;
			C := Source (Source_Cursor);
			if C = Carriage_Return then
				This.State := Have_CR;
			elsif C = Line_Feed then
				if This.State /= Have_CR then
					Target_Cursor := Target_Cursor + 1;
					Target (Target_Cursor) := Carriage_Return;
					if Target_Cursor >= Target'Last then -- Buffer ends while outputtting CR-LF
						This.State := Need_LF;
						return;
					end if;
					This.State := Initial;
				end if;
			end if;
			Target_Cursor := Target_Cursor + 1;
			Target (Target_Cursor) := C;
		end loop;
	end Process;

end Encodings.Line_Endings.Generic_Add_CR;
