with Ada.Assertions;
use  Ada.Assertions;
package body Encodings.Line_Endings.Generic_Strip_CR is

	overriding function In_Initial_State (State : Converter) return Boolean
	is (not State.Have_CR);

	procedure Reset (
		This : in out Converter)
	is
	begin
		This.Have_CR := False;
	end Reset;

	procedure Process (
		This          : in out Converter;
		Source        : in     String_Type;
		Source_Cursor :    out Natural;
		Target        :    out String_Type;
		Target_Cursor :    out Natural)
	is
		C : Character_Type;
	begin
		Source_Cursor := Source'First - 1;
		Target_Cursor := Target'First - 1;
		while Source_Cursor < Source'Last loop
			C := Source (Source_Cursor + 1);
			if This.Have_CR and C /= Line_Feed then -- emit CR not the part of CRLF sequence
				if Target_Cursor < Target'Last then
					Target_Cursor := Target_Cursor + 1;
					Target (Target_Cursor) := Carriage_Return;
				else
					return;
				end if;
				This.Have_CR := False;
			end if;
			if C = Carriage_Return then
				Assert (This.Have_CR = False, "Have should be cleared before or if condition shoudn't be true");
				This.Have_CR := True;
			else
				This.Have_CR := False;
				if Target_Cursor < Target'Last then
					Target_Cursor := Target_Cursor + 1;
					Target (Target_Cursor) := C;
				else
					return;
				end if;
			end if;
			Source_Cursor := Source_Cursor + 1;
		end loop;
	end Process;

end Encodings.Line_Endings.Generic_Strip_CR;
