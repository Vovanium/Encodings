package body Encodings.Unicode.UTF_8.From_UTF_32 is

	overriding function In_Initial_State (State : Converter) return Boolean
	is (State.L = 0);

	procedure Reset (State : in out Converter) is
	begin
		State.L := 0;
		State.Code := 0;
	end Reset;

	procedure Process (
		State       : in out Converter;
		Source      : in     UTF_32_String;
		Source_Last :    out Natural;
		Target      :    out UTF_8_String;
		Target_Last :    out Natural)
	is
		C : UTF_32_Character;
		Q : UTF_8_Character;
		use Interfaces;
	begin
		Source_Last := Source'First - 1;
		Target_Last := Target'First - 1;
		Main : while Target_Last < Target'Last loop
			if State.L > 0 then
				State.L := State.L - 1;
				Q := Make_Extension_Byte (State.Code, State.L);
			else
				exit Main when Source_Last >= Source'Last;
				Source_Last := Source_Last + 1;
				C := Source (Source_Last);
				case UTF_32_Character'Pos (C) is
				when 16#000000# .. 16#00007F# =>
					Q := UTF_8_Character'Val (UTF_32_Character'Pos (C));
				when 16#000080# .. 16#0007FF# =>
					State.Code := UTF_32_Character'Pos (C);
					State.L := 1;
					Q := Make_Initial_Byte (State.Code, State.L);
				when 16#000800# .. 16#00FFFF# =>
					State.Code := UTF_32_Character'Pos (C);
					State.L := 2;
					Q := Make_Initial_Byte (State.Code, State.L);
				when 16#010000# .. 16#10FFFF# =>
					State.Code := UTF_32_Character'Pos (C);
					State.L := 3;
					Q := Make_Initial_Byte (State.Code, State.L);
				when others =>
					raise Encoding_Error with "Out of unicode range";
				end case;
			end if;
			Target_Last := Target_Last + 1;
			Target (Target_Last) := Q;
		end loop Main;
	end Process;

end Encodings.Unicode.UTF_8.From_UTF_32;