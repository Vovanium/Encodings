package body Encodings.Generic_Single_Replace is

	function In_Initial_State (State : Converter) return Boolean
	is (State.S = Initial and then State.SI = Original'First);

	procedure Reset (
		This : in out Converter)
	is
	begin
		This.S := Initial;
		This.SI := Original'First;
		This.TI := Replacement'Last;
	end Reset;

	procedure Process (
		This          : in out Converter;
		Source        : in     String_Type;
		Source_Cursor :    out Natural;
		Target        :    out String_Type;
		Target_Cursor :    out Natural)
	is
		procedure Put (C : Character_Type) is
		begin
			Target_Cursor := Target_Cursor + 1;
			Target (Target_Cursor) := C;
		end Put;
		C : Character_Type;
	begin
		Source_Cursor := Source'First - 1;
		Target_Cursor := Target'First - 1;
		while Source_Cursor < Source'Last and Target_Cursor < Target'Last loop
			case This.S is
			when Initial =>
				Source_Cursor := Source_Cursor + 1;
				C := Source (Source_Cursor);
				if C = Original (This.SI) then -- original match
					if This.SI = Original'Last then -- full string matched
						case Replacement'Length is
						when 0 =>
							This.SI := Original'First;
						when 1 =>
							Put (Replacement (Replacement'First));
							This.SI := Original'First;
						when others =>
							Put (Replacement (Replacement'First));
							This.TI := Replacement'First;
							This.S := Output_Replacement;
						end case;
					else
						This.SI := This.SI + 1;
					end if;
				else -- no match
					if This.SI = Original'First then -- no matched prefix
						Put (C);
					else
						This.TI := Original'First;
						Put (Original (Original'First));
						This.C := C;
						This.S := Output_Original;
					end if;
				end if;
			when Output_Original =>
				This.TI := This.TI + 1;
				if This.TI = This.SI then -- whole matched part already written
					Put (This.C);
					This.SI := Original'First;
					This.S := Initial;
				else
					Put (Original (This.TI));
				end if;
			when Output_Replacement =>
				This.TI := This.TI + 1;
				Put (Replacement (This.TI));
				if This.TI = Replacement'Last then
					This.SI := Original'First;
					This.S := Initial;
				end if;
			end case;
		end loop;
	end Process;

end Encodings.Generic_Single_Replace;
