with Encodings.Generic_Converters;
package Encodings.Converters is
	package Narrow_to_Narrow is new Generic_Converters (
		Source_Character => Character,
		Target_Character => Character,
		Source_String    => String,
		Target_String    => String
	);

	package Narrow_to_Wide is new Generic_Converters (
		Source_Character => Character,
		Target_Character => Wide_Character,
		Source_String    => String,
		Target_String    => Wide_String
	);

	package Narrow_to_Wide_Wide is new Generic_Converters (
		Source_Character => Character,
		Target_Character => Wide_Wide_Character,
		Source_String    => String,
		Target_String    => Wide_Wide_String
	);

	package Wide_to_Narrow is new Generic_Converters (
		Source_Character => Wide_Character,
		Target_Character => Character,
		Source_String    => Wide_String,
		Target_String    => String
	);

	package Wide_to_Wide is new Generic_Converters (
		Source_Character => Wide_Character,
		Target_Character => Wide_Character,
		Source_String    => Wide_String,
		Target_String    => Wide_String
	);

	package Wide_to_Wide_Wide is new Generic_Converters (
		Source_Character => Wide_Character,
		Target_Character => Wide_Wide_Character,
		Source_String    => Wide_String,
		Target_String    => Wide_Wide_String
	);

	package Wide_Wide_to_Narrow is new Generic_Converters (
		Source_Character => Wide_Wide_Character,
		Target_Character => Character,
		Source_String    => Wide_Wide_String,
		Target_String    => String
	);

	package Wide_Wide_to_Wide is new Generic_Converters (
		Source_Character => Wide_Wide_Character,
		Target_Character => Wide_Character,
		Source_String    => Wide_Wide_String,
		Target_String    => Wide_String
	);

	package Wide_Wide_to_Wide_Wide is new Generic_Converters (
		Source_Character => Wide_Wide_Character,
		Target_Character => Wide_Wide_Character,
		Source_String    => Wide_Wide_String,
		Target_String    => Wide_Wide_String
	);
end Encodings.Converters;
