package body Encodings.Utility.Generic_Sequence_Buffers is

	procedure Copy (
		Source        : in     String_Type;
		Source_Cursor : in out Natural;
		Target        : in out String_Type;
		Target_Cursor : in out Natural)
	is
		Length : constant Natural := Natural'Min (
			Remaining_Length (Source, Source_Cursor),
			Remaining_Length (Target, Target_Cursor));
		New_Source_Cursor : constant Natural := Source_Cursor + Length;
		New_Target_Cursor : constant Natural := Target_Cursor + Length;
	begin
		if Length > 0 then
			Target (Target_Cursor + 1 .. New_Target_Cursor)
			 := Source (Source_Cursor + 1 .. New_Source_Cursor);
			Target_Cursor := New_Target_Cursor;
			Source_Cursor := New_Source_Cursor;
		end if;
	end Copy;

	--

	function Is_Empty (Buffer : in Sequence_Buffer) return Boolean
	is (Buffer.Last <= Buffer.Cursor);

	function Length (Buffer : Sequence_Buffer) return Natural
	is (Buffer.Last - Buffer.Cursor);

	procedure Write_Buffered (
		Buffer        : in out Sequence_Buffer;
		Target        : in out String_Type;
		Target_Cursor : in out Natural
	) is
	begin
		Copy (Buffer.Data (Buffer.Data'First .. Buffer.Last), Buffer.Cursor, Target, Target_Cursor);
	end Write_Buffered;

	procedure Set_Buffer (
		Buffer : in out Sequence_Buffer;
		Source : in     String_Type
	) is
	begin
		Buffer.Data (Buffer.Data'First .. Buffer.Data'First - 1 + Source'Length) := Source;
		Buffer.Cursor := Buffer.Data'First;
		Buffer.Last := Buffer.Data'First - 1 + Source'Length;
	end Set_Buffer;

	procedure Write (
		Buffer        : in out Sequence_Buffer;
		Source        : in     String_Type;
		Target        : in out String_Type;
		Target_Cursor : in out Natural
	) is
		Source_Cursor : Natural := 0;
	begin
		Copy (Source, Source_Cursor, Target, Target_Cursor);
		Set_Buffer (Buffer, Source (Source_Cursor + 1 .. Source'Last));
	end Write;

end Encodings.Utility.Generic_Sequence_Buffers;