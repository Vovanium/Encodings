with Encodings.Converters;
with Encodings.Unicode.UTF_32;
use  Encodings.Unicode.UTF_32;

package Encodings.Unicode.UTF_8.From_UTF_32 is

	type Converter is new Converters.Wide_Wide_to_Narrow.Converter with private;

	overriding function In_Initial_State (State : Converter) return Boolean;

	overriding procedure Reset (State : in out Converter);

	overriding procedure Process (
		State       : in out Converter;
		Source      : in     UTF_32_String;
		Source_Last :    out Natural;
		Target      :    out UTF_8_String;
		Target_Last :    out Natural);

private
	type Converter is new Converters.Wide_Wide_to_Narrow.Converter
	with record
		L    : Sequence_Count  := 0; -- Remaining
		Code : Code_Point      := 0;
	end record;

end Encodings.Unicode.UTF_8.From_UTF_32;