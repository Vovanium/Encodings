generic
	type Source_Character is (<>);
	type Target_Character is (<>);
	type Source_String is array (Positive range <>) of Source_Character'Base;
	type Target_String is array (Positive range <>) of Target_Character'Base;
package Encodings.Generic_Converters with Pure is
	type Converter is interface;

	function In_Initial_State (This : Converter) return Boolean is abstract;
	-- Query if converter in initial state, :
	-- both source and target stream in their initial states
	-- (as defined in their encoding specifiactions)
	-- and there's no data in internal buffers that have to be output

	procedure Reset (This : in out Converter) is null;
	--  Reset the converter to the initial state (if any)

	procedure Process (
		This          : in out Converter;      -- Converter
		Source        : in     Source_String;  -- Character sequence to convert
		Source_Cursor :    out Natural;        -- Last character consumed by converter
		Target        :    out Target_String;  -- Converted character sequence
		Target_Cursor :    out Natural         -- Last character in converted string
	) is abstract;
	--  Procedure converts a (part of) string
	--  Procedure must consume whole source string or
	--  fill whole target string (or both).
	--  Source (Source_Cursor + 1 .. Source'Last)

	--  Note: this is most basic conversion interface
	--  It is not require memory allocation or
	--  preliminary length calculation

end  Encodings.Generic_Converters;