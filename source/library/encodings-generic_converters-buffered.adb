package body Encodings.Generic_Converters.Buffered is

	procedure Start (
		State         : in out Buffered_Converter;
		Source        : in     Source_String;
		Source_Cursor :    out Natural;
		Target        :    out Target_String;
		Target_Cursor :    out Natural)
	is
	begin
		Source_Cursor := Source'First - 1;
		Target_Cursor := Target'First - 1;
		Buffers.Write_Buffered (State.Buffer, Target, Target_Cursor);
	end Start;

	not overriding procedure Write (
		State         : in out Buffered_Converter;
		Data          : in     Target_String;
		Target        : in out Target_String;
		Target_Cursor : in out Natural)
	is
	begin
		Buffers.Write (State.Buffer, Data, Target, Target_Cursor);
	end Write;

end Encodings.Generic_Converters.Buffered;