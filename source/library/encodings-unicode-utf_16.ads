package Encodings.Unicode.UTF_16 is
	subtype UTF_16_Character is Wide_Character;

	subtype High_Surrogate is UTF_16_Character
		range UTF_16_Character'Val (16#D800#) .. UTF_16_Character'Val (16#DBFF#);
	subtype Low_Surrogate is UTF_16_Character
		range UTF_16_Character'Val (16#DC00#) .. UTF_16_Character'Val (16#DFFF#);

	subtype UTF_16_String is Ada.Strings.UTF_Encoding.UTF_16_Wide_String;

	Byte_Order_Mark : constant UTF_16_String := (
		1 => UTF_16_Character'Val (Byte_Order_Mark_Code));

private
	use Interfaces;

	function Make_High_Surrogate (C : Code_Point) return UTF_16_Character
	is (UTF_16_Character'Val (Shift_Right (C - 16#010000#, 10) or 16#D800#))
	with
		Pre => C not in Basic_Multilingual_Plane,
		Post => Make_High_Surrogate'Result in High_Surrogate;

	function Make_Low_Surrogate (C : Code_Point) return UTF_16_Character
	is (UTF_16_Character'Val ((C and 16#03FF#) or 16#DC00#))
	with
		Pre => C not in Basic_Multilingual_Plane,
		Post => Make_Low_Surrogate'Result in Low_Surrogate;

end Encodings.Unicode.UTF_16;
