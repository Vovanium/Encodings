with Encodings.Utility.Generic_Sequence_Buffers;

generic
	Buffer_Length : Positive := 8;
package Encodings.Generic_Converters.Buffered with Pure is

	type Buffered_Converter is abstract new Converter with private;

private
	package Buffers is new Encodings.Utility.Generic_Sequence_Buffers (
		Character_Type => Target_Character,
		String_Type    => Target_String,
		Max_Length     => Buffer_Length);

	type Buffered_Converter is abstract new Converter with record
		Buffer : Buffers.Sequence_Buffer;
	end record;
	function Buffered_Length (State : Buffered_Converter) return Natural is
		(Buffers.Length (State.Buffer));

	not overriding procedure Start (
		State         : in out Buffered_Converter;
		Source        : in     Source_String;
		Source_Cursor :    out Natural;
		Target        :    out Target_String;
		Target_Cursor :    out Natural)
	with
		Pre => Target'Length > 0,
		Post => Buffered_Length (State) = 0 or Target_Cursor = Target'Last;
	-- This should be called at the start of `Process` procedure
	-- to initialize variables and flush buffered data

	not overriding procedure Write (
		State         : in out Buffered_Converter;
		Data          : in     Target_String;
		Target        : in out Target_String;
		Target_Cursor : in out Natural)
	with
		Pre  => Buffered_Length (State) = 0
		  and Target_Cursor in Target'First - 1 .. Target'Last - 1,
		Post => Target_Cursor in Target'First - 1 .. Target'Last
		  and Target_Cursor - Target_Cursor'Old + Buffered_Length (State) = Data'Length
		  and (Buffered_Length (State) = 0 or Target_Cursor = Target'Last);
	-- Puts a string to the output and buffers its part if required

end Encodings.Generic_Converters.Buffered;