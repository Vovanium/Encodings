package body Encodings.Unicode.UTF_16.From_UTF_32 is

	function In_Initial_State (State : Converter) return Boolean
	is (State.Surrogate not in Low_Surrogate);

	procedure Reset (State : in out Converter) is
	begin
		State.Surrogate := UTF_16_Character'Val (0);
	end Reset;

	procedure Process (
		State       : in out Converter;
		Source      : in     UTF_32_String;
		Source_Last :    out Natural;
		Target      :    out UTF_16_String;
		Target_Last :    out Natural)
	is
		Code : Code_Point;
		Q    : UTF_16_Character;
		use Interfaces;
	begin
		Source_Last := Source'First - 1;
		Target_Last := Target'First - 1;
		Main : while Target_Last < Target'Last loop
			if State.Surrogate in Low_Surrogate then
				Q := State.Surrogate;
				State.Surrogate := UTF_16_Character'Val (0);
			else
				exit Main when Source_Last >= Source'Last;
				Source_Last := Source_Last + 1;
				Code := UTF_32_Character'Pos (Source (Source_Last));
				if Code <= 16#FFFF# then -- BMP
					Q := UTF_16_Character'Val (Code);
				else
					Q := Make_High_Surrogate (Code);
					State.Surrogate := Make_Low_Surrogate (Code);
				end if;
			end if;
			Target_Last := Target_Last + 1;
			Target (Target_Last) := Q;
		end loop Main;
	end Process;

end Encodings.Unicode.UTF_16.From_UTF_32;
