package Encodings.Unicode.UTF_32 is
	subtype UTF_32_Character is Wide_Wide_Character
		range Wide_Wide_Character'Val (0) .. Wide_Wide_Character'Val (Code_Point_Last);

	subtype UTF_32_String is Wide_Wide_String;

end Encodings.Unicode.UTF_32;