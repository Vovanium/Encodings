package Encodings.Unicode.UTF_8 is
	subtype UTF_8_Character
		is Character range Character'Val (0) .. Character'Val (16#F4#);
	subtype Single_Byte_Character
		is UTF_8_Character range UTF_8_Character'Val (0) .. UTF_8_Character'Val (16#7F#);
	subtype Extension_Byte
		is UTF_8_Character range UTF_8_Character'Val (16#80#) .. UTF_8_Character'Val (16#B8#);
	--  C0 .. C1 -- illegal, encoding non-shortest sequence for 00 .. 7F
	subtype Initial_2_Byte
		is UTF_8_Character range UTF_8_Character'Val (16#C2#) .. UTF_8_Character'Val (16#DF#);
	subtype Initial_3_Byte
		is UTF_8_Character range UTF_8_Character'Val (16#E0#) .. UTF_8_Character'Val (16#EF#);
	subtype Initial_4_Byte
		is UTF_8_Character range UTF_8_Character'Val (16#F0#) .. UTF_8_Character'Val (16#F4#);
	--  F5 .. F7 encoding code points out of range
	--  F8 .. FD are deprecated multibyte sequences
	--  FE .. FF illegal

	subtype UTF_8_String is Ada.Strings.UTF_Encoding.UTF_8_String;

	Byte_Order_Mark : constant UTF_8_String := (
		UTF_8_Character'Val (16#EF#),
		UTF_8_Character'Val (16#BB#),
		UTF_8_Character'Val (16#BF#));

private
	Extension_Byte_Mask : constant := 16#3F#;
	Extension_Byte_Base : constant := 16#80#;

	use Interfaces;

	function Make_Extension_Byte (
		P : Code_Point;
		R : Natural) -- Remaining bytes (index from the last)
		return Extension_Byte
	is (UTF_8_Character'Val ((Interfaces.Shift_Right (P, 6 * R) and Extension_Byte_Mask)
		or Extension_Byte_Base))
	with
		Pre => P not in 0 .. 16#7F# and R < 3;

	function Initial_Mask (L : Positive) return Code_Point
	is (Shift_Left (1, 6 - L) - 1);

	function Initial_Base (L : Positive) return Code_Point
	is (16#100# - Shift_Left (1, 7 - L));

	function Make_Initial_Byte (
		P : Code_Point;
		L : Positive) -- Extension bytes
		return UTF_8_Character
	is (UTF_8_Character'Val (Shift_Right (P, 6 * L) or Initial_Base (L)))
	with
		Pre => P not in 0 .. 16#7F# and L < 4,
		Post => Make_Initial_Byte'Result in Initial_2_Byte | Initial_3_Byte | Initial_4_Byte;
	-- Masking is not needed here

end Encodings.Unicode.UTF_8;
