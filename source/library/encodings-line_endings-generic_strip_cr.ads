with Encodings.Generic_Converters;

generic
	type Character_Type is (<>); -- Character, Wide_Character, Wide_Wide_Character (or whatever)
	type String_Type    is array (Positive range <>) of Character_Type'Base;
	Carriage_Return     : in Character_Type; -- CR in the corresponding type
	Line_Feed           : in Character_Type; -- LF in the corresponding type
	with package Converter_Base is new Encodings.Generic_Converters (
		Source_Character => Character_Type,
		Target_Character => Character_Type,
		Source_String    => String_Type,
		Target_String    => String_Type); -- Base package
package Encodings.Line_Endings.Generic_Strip_CR is
	type Converter is new Converter_Base.Converter with private;

	overriding function In_Initial_State (State : Converter) return Boolean;

	overriding procedure Reset (
		This : in out Converter);

	overriding procedure Process (
		This          : in out Converter; -- Coder state
		Source        : in     String_Type; -- String to be converted
		Source_Cursor :    out Natural; -- Last index of source string read
		Target        :    out String_Type; -- Converted string
		Target_Cursor :    out Natural); -- Last Index of destination string written
private
	type Converter is new Converter_Base.Converter with record
		Have_CR : Boolean := False;
	end record;
end Encodings.Line_Endings.Generic_Strip_CR;
