--  Adds carriage return (0Dh) before line feed (0Ah) if it does not present
package Encodings.Line_Endings.CR_LF with Pure is
end Encodings.Line_Endings.CR_LF;