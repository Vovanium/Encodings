generic
	type Character_Type is (<>);
	type String_Type    is array (Positive range <>) of Character_Type'Base;
	Max_Length          : Positive;
package Encodings.Utility.Generic_Sequence_Buffers with Pure is

	-- Buffering character sequences

	-- Note on string processing
	-- prcessed strings represented as data itself and
	-- a position counter pointing to the last position processed
	-- So a processed part is from the beginning of a string until the counter.
	-- An unprocessed is from the position next to a counter till the end.
	-- The counter is normally named with _Cursor suffix.
	--
	-- First unprocessed position would have Last + 1 range thus may overflow
	-- if Last is Integer'Last.
	--
	--
	--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	--    |  Already processed part   |   Unprocessed yet part  |
	--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	--     ^                         ^                         ^
	--    'First                   Cursor                   'Last


	function Remaining_Length (
		Data   : String_Type; -- A data (only its bounds are processed)
		Cursor : Natural      -- A position counter
	) return Natural is (
		Data'Last - Cursor
	) with
		Pre => Cursor in Data'First - 1 .. Data'Last,
		Post => Remaining_Length'Result in 0 .. Data'Length;
	-- Length of a data remaining unprocessed
	-- @return number of data items unprocessed

	function Is_Exhausted (
		Data   : String_Type; -- A data (only its bounds are processed)
		Cursor : Natural      -- A position counter
	) return Boolean is (
		Data'Last = Cursor
	) with
		Pre => Cursor in Data'First - 1 .. Data'Last;
	-- Test if no more unprocessed data
	-- @return True if no unprocessed data left (or even none was)

	procedure Copy (
		Source        : in     String_Type; -- Source data
		Source_Cursor : in out Natural;     -- Source data position counter
		Target        : in out String_Type; -- Target space
		Target_Cursor : in out Natural)     -- Target space position counter
	with
		Pre => Source_Cursor in Source'First - 1 .. Source'Last
		  and Target_Cursor in Target'First - 1 .. Target'Last,
		Post => Source_Cursor in Source'First - 1 .. Source'Last
		  and Target_Cursor in Target'First - 1 .. Source'Last
		  and Source_Cursor - Source_Cursor'Old = Target_Cursor - Target_Cursor'Old
		  and Source (Source_Cursor'Old + 1 .. Source_Cursor)
		    = Target (Target_Cursor'Old + 1 .. Target_Cursor)
		  and (Source_Cursor = Source'Last or Target_Cursor = Target'Last);
	-- Copy as much as possible data from a source to a destination.
	-- Copying starts at positions following thi position counters and
	-- ends when the end of the source data or
	-- the end of the target space reached

	--

	type Sequence_Buffer is private;

	function Is_Empty (
		Buffer : Sequence_Buffer -- a buffer to test
	) return Boolean;
	-- Test if the buffer empty
	-- @return True if buffer is empty

	function Length (
		Buffer : Sequence_Buffer -- a buffer
	) return Natural with
		Post => Length'Result in 0 .. Max_Length;
	-- Length of a data buffered
	-- @return Number of elements buffered

	procedure Write_Buffered (
		Buffer        : in out Sequence_Buffer; -- the buffer
		Target        : in out String_Type;     -- string to write in
		Target_Cursor : in out Natural          -- writing starts after this position
	) with
		Pre => Target_Cursor in Target'First - 1 .. Target'Last,
		Post => Target_Cursor in Target'First - 1 .. Target'Last
		  and Length (Buffer'Old) - Length (Buffer) = Target_Cursor - Target_Cursor'Old
		  and (Is_Empty (Buffer) or Target_Cursor = Target'Last);
	-- Write buffered data to a target string

	procedure Set_Buffer (
		Buffer : in out Sequence_Buffer; -- the buffer
		Source : in     String_Type      -- data to store
	) with
		Pre => Is_Empty (Buffer),
		Post => Length (Buffer) = Source'Length;
	-- Store a data in a buffer

	procedure Write (
		Buffer        : in out Sequence_Buffer;
		Source        : in     String_Type;
		Target        : in out String_Type;
		Target_Cursor : in out Natural)
	with
		Pre => Is_Empty (Buffer);

private

	type Sequence_Buffer is record
		Data   : String_Type (1 .. Max_Length) := (others => Character_Type'First);
		Cursor : Natural range 0 .. Max_Length := 0; -- one before first position of buffered data
		Last   : Natural range 0 .. Max_Length := 0; -- last position of total buffered data
	end record with
		Type_Invariant => Cursor <= Last;

end Encodings.Utility.Generic_Sequence_Buffers;
