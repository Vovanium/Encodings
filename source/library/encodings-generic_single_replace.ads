--
--  Performs single string replacement. Useful to make CR-LF translation.
--

with Encodings.Generic_Converters;

generic
	type Character_Type is (<>); -- Character, Wide_Character, Wide_Wide_Character (or whatever)
	type String_Type    is array (Positive range <>) of Character_Type'Base;
	Original            : in String_Type; -- Source string to replace
	Replacement         : in String_Type; -- Replacement string
	with package Converter_Base is new Encodings.Generic_Converters (
		Source_Character => Character_Type,
		Target_Character => Character_Type,
		Source_String    => String_Type,
		Target_String    => String_Type); -- Base package
package Encodings.Generic_Single_Replace is

	type Converter is new Converter_Base.Converter with private;

	overriding function In_Initial_State (State : Converter) return Boolean;

	overriding procedure Reset (
		This : in out Converter);

	overriding procedure Process (
		This          : in out Converter; -- Converter state
		Source        : in     String_Type; -- String to be converted
		Source_Cursor :    out Natural; -- Last index of source string read
		Target        :    out String_Type; -- Converted string
		Target_Cursor :    out Natural); -- Last Index of destination string written

private

	type State is (
		Initial,              -- matching / copying
		Output_Original,      -- partial original match, outputting matched part then unmatched character
		Output_Replacement);  -- full original match, outputting replacement

	type Converter is new Converter_Base.Converter with record
		S  : State   := Initial;
		SI : Natural := Original'First; -- Source string match index
		TI : Natural := Replacement'Last; -- Target string output index
		C  : Character_Type; -- Character match is failed on
	end record;

end Encodings.Generic_Single_Replace;
